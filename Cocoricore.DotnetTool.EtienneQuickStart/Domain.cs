using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using Cocoricore.DotnetTool.Lib;

namespace Cocoricore.DotnetTool.EtienneQuickStart
{
    public class Domain
    {
        private readonly IWorkingDirectory workingDirectory;
        private readonly Configuration conf;
        private readonly DotNet dotnet;
        private readonly MustacheTool mustache;
        private readonly ILanguageService languageService;

        public Domain(
            IWorkingDirectory workingDirectory,
            Configuration conf,
            DotNet dotnet,
            MustacheTool mustache,
            ILanguageService languageService)
        {
            this.workingDirectory = workingDirectory;
            this.conf = conf;
            this.dotnet = dotnet;
            this.mustache = mustache;
            this.languageService = languageService;
        }

        public void Generate()
        {
            var framework = "-f " + conf.DotNetFramework;
            var domain = $"{conf.ProjectName}.{languageService.Domain}";
            var domainTest = $"{domain}.Test";

            dotnet.Cmd($"new classlib -o {domain} {framework}");
            dotnet.Cmd($"sln add {domain}");

            dotnet.Cmd($"new xunit -o {domainTest} {framework}");
            dotnet.Cmd($"sln add {domainTest}");

            dotnet.Cmd($"add {domainTest}  reference {domain}");

            mustache.Generate(
                "{{ProjectName}}.{{Domain}}",
                "MessageBus/MessageBus.cs",
                "MessageBus/HandlerFinder.cs",
                "Service/IUserService.cs",
                "AssemblyInfo.cs"
            );
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using Cocoricore.DotnetTool.Lib;

namespace Cocoricore.DotnetTool.EtienneQuickStart
{

    public class Generator
    {
        private readonly Configuration conf;
        private readonly DotNet dotnet;
        private readonly CocoriCoreDependency cocoriCore;
        private readonly MustacheTool mustache;
        private readonly Domain domain;
        private readonly WebApi webApi;

        public Generator(
            Configuration conf,
            DotNet dotnet,
            CocoriCoreDependency cocoriCore,
            MustacheTool mustache,
            Domain domain,
            WebApi webApi)
        {
            this.conf = conf;
            this.dotnet = dotnet;
            this.cocoriCore = cocoriCore;
            this.mustache = mustache;
            this.domain = domain;
            this.webApi = webApi;
        }

        public void New()
        {
            dotnet.Cmd("new sln");

            mustache.Generate(
                "",
                ".vscode/settings.json",
                "Dockerfile",
                ".gitignore"
            );

            //AddNugetConfig(conf);
            cocoriCore.SetupSourcesIfNeeded();
            domain.Generate();
            if (!conf.DomainOnly)
                webApi.Generate();
            cocoriCore.UpdateReferences();
        }

        private void AddNugetConfig(string directory)
        {
            var text = @"
<?xml version=""1.0"" encoding=""utf-8""?>
<configuration>
  <packageSources>
    <add key=""nuget.org"" value=""https://api.nuget.org/v3/index.json"" protocolVersion=""3"" />
    <!-- <add key=""Local Packages"" value=""\\tfsbuild2017\Partage\Nuget"" /> -->
	<add key=""Local Packages"" value=""..\..\LocalNuget"" />
  </packageSources>
  <disabledPackageSources>
    <add key=""Microsoft and .NET"" value=""true"" />
  </disabledPackageSources>
</configuration>";
            File.WriteAllText(text, Path.Combine(directory, "NuGet.config"));

        }

        public void AddEntity(string name)
        {
            mustache.Generate(
                new
                {
                    EntityName = name
                },
                "{{ProjectName}}.{{Domain}}/{{Entity}}",
                "{{EntityName}}.cs"
            );
        }

        public void AddQuery(string name)
        {
            var data = new
            {
                QueryName = name,
                WithEntity = true,
                EntityName = name
            };
            mustache.Generate(
                data,
                "{{ProjectName}}.{{Domain}}/{{Read}}",
                "{{QueryName}}.cs"
            );

            mustache.Generate(
                data,
                "{{ProjectName}}.{{Domain}}.Test/{{Read}}",
                "Test{{QueryName}}.cs"
            );
        }

        public void AddCommand(string name)
        {
            var data = new
            {
                CommandName = name,
                WithEntity = false,
                EntityName = name
            };

            mustache.Generate(
                data,
                "{{ProjectName}}.{{Domain}}/{{Write}}",
                "{{CommandName}}.cs"
            );

            mustache.Generate(
                data,
                "{{ProjectName}}.{{Domain}}.Test/{{Write}}",
                "Test{{CommandName}}.cs"
            );
        }
    }
}

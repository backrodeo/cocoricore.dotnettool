using System.Reflection;
using Cocoricore.DotnetTool.Lib;
using Ninject;

namespace Cocoricore.DotnetTool.EtienneQuickStart
{
    public class Injection
    {

        public static Generator Instantiate(Configuration conf, string workingDirectory)
        {
            var kernel = new StandardKernel();
            kernel.Bind<IWorkingDirectory>().ToConstant(new WorkingDirectory(workingDirectory));
            kernel.Bind<Configuration>().ToConstant(conf);
            kernel.Bind<CocoriCoreDependencyConfiguration>().ToConstant(conf.CocoriCore);
            kernel.Bind<InjectionLibrary>().ToConstant(conf.InjectionLibrary);
            kernel.Bind<IProjectName>().ToConstant(new IProjectName() { ProjectName = conf.ProjectName });
            kernel.Bind<MustacheToolConfiguration>().ToConstant(new MustacheToolConfiguration() { Assembly = AssemblyInfo.Assembly });

            if (conf.Language == Language.En)
                kernel.Bind<ILanguageService>().To<LanguageServiceEn>();
            else if (conf.Language == Language.Fr)
                kernel.Bind<ILanguageService>().To<LanguageServiceFr>();

            return kernel.Get<Generator>();
        }
    }
}
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using Cocoricore.DotnetTool.Lib;

namespace Cocoricore.DotnetTool.EtienneQuickStart
{
    public class WebApi
    {
        private readonly IWorkingDirectory workingDirectory;
        private readonly Configuration conf;
        private readonly DotNet dotnet;
        private readonly ILanguageService languageService;
        private readonly MustacheTool mustache;

        public WebApi(
            IWorkingDirectory workingDirectory,
            Configuration conf,
            DotNet dotnet,
            MustacheTool mustache,
            ILanguageService languageService)
        {
            this.workingDirectory = workingDirectory;
            this.conf = conf;
            this.dotnet = dotnet;
            this.languageService = languageService;
            this.mustache = mustache;
        }

        public void Generate()
        {
            var framework = "-f " + conf.DotNetFramework;
            var webapi = $"{conf.ProjectName}.WebApi";
            var domain = $"{conf.ProjectName}.{languageService.Domain}";
            var webapitest = $"{webapi}.Test";

            dotnet.Cmd($"new web -o {webapi} {framework}");
            dotnet.Cmd($"sln add {webapi}");

            dotnet.Cmd($"new xunit -o {webapitest} {framework}");
            dotnet.Cmd($"sln add {webapitest}");

            dotnet.Cmd($"add {webapi} reference {domain}");

            if (conf.InjectionLibrary == InjectionLibrary.Ninject)
            {
                dotnet.Cmd($"add {webapi} reference CocoriCore/backend/CocoriCore.Ninject");
                dotnet.Cmd($"add {webapi} package Ninject");
                dotnet.Cmd($"add {webapi} package Ninject.Extensions.ContextPreservation");
                dotnet.Cmd($"add {webapi} package Ninject.Extensions.NamedScope");
            }

            dotnet.Cmd($"add {webapitest} reference {domain}");
            dotnet.Cmd($"add {webapitest} reference {webapi}");

            if (conf.InjectionLibrary == InjectionLibrary.Ninject)
            {
                dotnet.Cmd($"add {webapitest} reference CocoriCore/backend/CocoriCore.Ninject");
                dotnet.Cmd($"add {webapitest} package Ninject");
                dotnet.Cmd($"add {webapitest} package Ninject.Extensions.ContextPreservation");
                dotnet.Cmd($"add {webapitest} package Ninject.Extensions.NamedScope");
            }

            mustache.Generate(
                "{{ProjectName}}.WebApi",
                "ApplicationMiddleware/AppErrorBus.cs",
                "ApplicationMiddleware/AppHttpErrorWriter.cs",
                "ApplicationMiddleware/AppHttpResponseWriter.cs",
                "ApplicationMiddleware/ApplicationMiddleware.cs",
                "ApplicationMiddleware/AppTracer.cs",
                "ApplicationMiddleware/HttpTrace.cs",
                "ApplicationMiddleware/IAppErrorBus.cs",
                "ApplicationMiddleware/IAppTracer.cs",
                "Injection/WebApiModule.cs",
                "Injection/WebApiModuleDev.cs",
                "Injection/WebApiModuleRecette.cs",
                "Route/ApiFootprintHandler.cs",
                "Route/RouterConfiguration.cs",
                "Service/UserService.cs",
                "Program.cs",
                "Startup.cs"
            );

            mustache.Generate(
                "{{ProjectName}}.WebApi.Test",
                "TestGenerateFootprint.cs"
            );
        }
    }
}
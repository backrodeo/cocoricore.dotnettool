﻿using Cocoricore.DotnetTool.Lib;

namespace Cocoricore.DotnetTool.EtienneQuickStart
{
    public class Configuration : GeneratorConfigurationBase
    {
        public Configuration() : base("EtienneQuickStart")
        {
        }

        public string DotNetFramework = "netcoreapp2.2";
        public Language Language = Language.En;
        public string ProjectName;
        public InjectionLibrary InjectionLibrary = InjectionLibrary.Ninject;
        public CocoriCoreDependencyConfiguration CocoriCore = new CocoriCoreDependencyConfiguration();
        public bool DomainOnly;
    }
}

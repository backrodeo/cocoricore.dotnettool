namespace Cocoricore.DotnetTool.Lib
{
    public class GeneratorConfiguration
    {
        public string DotNetFramework = "netcoreapp2.2";
        public string WorkingDirectory;
        public string ProjectName;
        public CocoriCoreDependencyConfiguration CocoriCore = new CocoriCoreDependencyConfiguration();
        public InjectionLibrary InjectionLibrary;

        public bool Cors = false;

        // Project structure
        public bool ReadAndWriteProjects = false;
        public bool ResourceProject = false;
        public bool ProjectionProject = false;
        public bool DerivationProject = false;
        public bool TestSubDirectory = true;
        // a project that contains only Program, Startup and application setings
        public bool StartupProject = false;
    }
}
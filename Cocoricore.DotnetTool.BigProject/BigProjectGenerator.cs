﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;

namespace Cocoricore.DotnetTool.Lib
{

    public class BigProjectGenerator
    {
        private DotNet dotnet;
        private CocoriCoreDependency cocoriCore;
        private readonly BigProjectApplicationGenerator application;
        private readonly BigProjectApplicationTestGenerator applicationTest;
        private readonly BigProjectApiGenerator api;

        public BigProjectGenerator(
            BigProjectApiGenerator api,
            BigProjectApplicationGenerator application,
            BigProjectApplicationTestGenerator applicationTest
            )
        {
            this.api = api;
            this.application = application;
            this.applicationTest = applicationTest;
        }

        public void Generate(GeneratorConfiguration conf)
        {
            /*
            dotnet.WorkingDirectory(conf.WorkingDirectory);
            cocoriCore.Configure(conf);
            application.Configure(conf);
            applicationTest.Configure(conf);
            api.Configure(conf);
            
            dotnet.Cmd("new sln");
            cocoriCore.SetupSourcesIfNeeded();
            application.Generate();
            api.Generate();
            applicationTest.Generate();
            cocoriCore.UpdateReferences();
            */
        }
    }
}

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;

namespace Cocoricore.DotnetTool.Lib
{
    public class BigProjectApiGenerator
    {
        GeneratorConfiguration conf;
        private readonly MustacheTool mustache;
        DotNet dotnet;
        InjectionDependency injectionDependency;

        public BigProjectApiGenerator(GeneratorConfiguration conf, MustacheTool mustache)
        {
            this.conf = conf;
            this.mustache = mustache;
            //this.dotnet = new DotNet().WorkingDirectory(conf.WorkingDirectory);
            //this.injectionDependency = new InjectionDependency().Configure(conf);
        }

        public void Generate()
        {
            /*
            var framework = " -f " + conf.DotNetFramework;

            dotnet.Cmd("new classlib -o " + names.Api + framework);
            dotnet.Cmd("sln add " + names.Api);

            dotnet.Cmd("add " + names.Api + " reference " + names.ApplicationConfiguration);
            dotnet.Cmd("add " + names.Api + " reference " + names.ApplicationDomain);
            if (conf.ReadAndWriteProjects)
            {
                dotnet.Cmd("add " + names.Api + " reference " + names.ApplicationRead);
                dotnet.Cmd("add " + names.Api + " reference " + names.ApplicationWrite);
            }

            this.injectionDependency.AddDepencyTo(names.Api);


            //var mustache = new Mustache("BigProject.Files.Api", conf.WorkingDirectory, names.Api);

            mustache.Generate(
                "DIRECTORY",
                "Configuration/ErrorBusConfiguration.cs",
                "Configuration/HttpErrorWriterConfiguration.cs",
                "Configuration/HttpResponseWriterConfiguration.cs",
                "Configuration/InfrastructureConfiguration.cs",
                "Configuration/IoCApi.cs",
                "Configuration/MiddlewareConfiguration.cs",
                "Configuration/RepositoryConfiguration.cs",
                "Configuration/RouterConfiguration.cs",
                "Router.cs",
                "Startup.cs");
            */
        }


    }
}
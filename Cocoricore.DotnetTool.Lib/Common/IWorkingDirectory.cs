namespace Cocoricore.DotnetTool.Lib
{
    public interface IWorkingDirectory
    {
        string Directory { get; }
    }

    public class WorkingDirectory : IWorkingDirectory
    {
        public WorkingDirectory(string dir)
        {
            this.Directory = dir;
        }

        public string Directory { get; set; }
    }
}
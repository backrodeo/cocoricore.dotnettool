using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;

namespace Cocoricore.DotnetTool.Lib
{
    public class LanguageServiceEn : ILanguageService
    {
        public string Domain => "Domain";
        public string Read => "Read";
        public string Write => "Write";
        public string Entity => "Entity";
    }
}
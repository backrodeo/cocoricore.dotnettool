
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;

namespace Cocoricore.DotnetTool.Lib
{
    public class CocoriCoreDependency
    {
        //const string GitUrl = "https://backrodeo@bitbucket.org/llafitte007/cocoricore.git";
        const string GitUrl = "https://bitbucket.org/llafitte007/cocoricore.git";


        private CocoriCoreDependencyConfiguration conf;
        private readonly IWorkingDirectory workingDirectory;
        private readonly DotNet dotnet;
        private readonly Git git;

        public CocoriCoreDependency(
            CocoriCoreDependencyConfiguration conf,
            IWorkingDirectory workingDirectory,
            DotNet dotnet,
            Git git)
        {
            this.conf = conf;
            this.workingDirectory = workingDirectory;
            this.dotnet = dotnet;
            this.git = git;
        }

        public void SetupSourcesIfNeeded()
        {
            if (!conf.Package)
            {
                if (conf.Git)
                    this.GitClone();
                else if (conf.Directory != null && conf.Directory.Length > 0)
                    this.DirectoryCopy(conf.Directory);

                dotnet.Cmd("sln add CocoriCore/backend/CocoriCore");
            }
        }

        public void UpdateReferences()
        {
            if (conf.Package)
                this.AsPackage();
            else
                this.AsReference();
        }

        void GitClone()
        {
            var destDirectory = Path.Combine(workingDirectory.Directory, "CocoriCore");
            DirectoryExtension.EnsureEmpty(destDirectory);
            git.Clone(GitUrl, destDirectory);

            AsReference("./CocoriCore/backend/CocoriCore");
        }

        void DirectoryCopy(string directory)
        {
            var destDirectory = Path.Combine(workingDirectory.Directory, "CocoriCore");
            DirectoryExtension.EnsureEmpty(destDirectory);
            DirectoryExtension.DirectoryCopy(directory, destDirectory, true);
        }

        void AsReference()
        {
            AsReference("CocoriCore/backend/CocoriCore/CocoriCore.csproj");
        }

        void AsPackage()
        {
            throw new NotImplementedException("CocoricoreAsPackage");
        }

        void AsReference(string cocoricoreDir)
        {
            var projectsDir = FindCsProjSubDirectories();
            foreach (var fullDir in projectsDir)
            {
                var d = Path.GetFileName(fullDir);
                dotnet.Try("add " + d + " reference " + cocoricoreDir);
                //dotnet.Cmd(Path.Combine(this.workingDirectory, d))
            }
        }


        /*
                void CocoricoreClean(string cocoricoreDir)
                {
                    var projectsDir = FindCsProjSubDirectories();
                    foreach (var d in projectsDir)
                    {
                        Trydotnet.Cmd("remove " + d + " reference " + cocoricoreDir);
                        //Trydotnet.Cmd("remove " + d + " package " + "CocoriCore");

                        //dotnet.Cmd(Path.Combine(this.workingDirectory, d))
                    }
                }
        */
        List<string> FindCsProjSubDirectories()
        {
            var subDirectories = Directory.GetDirectories(workingDirectory.Directory);
            return subDirectories.Where(d => Directory.GetFiles(d, "*.csproj").Any()).ToList();
        }


    }
}
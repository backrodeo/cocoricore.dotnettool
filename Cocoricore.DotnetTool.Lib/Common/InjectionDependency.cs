
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;

namespace Cocoricore.DotnetTool.Lib
{
    class InjectionDependency
    {
        private readonly DotNet dotnet;
        private readonly InjectionLibrary injectionLibrary;

        public InjectionDependency(DotNet dotnet, InjectionLibrary injectionLibrary)
        {
            this.dotnet = dotnet;
            this.injectionLibrary = injectionLibrary;
        }

        public void AddDepencyTo(string projectName)
        {
            if (this.injectionLibrary == InjectionLibrary.Ninject)
            {
                dotnet.Cmd("add " + projectName + " reference CocoriCore/backend/CocoriCore.Ninject");
                dotnet.Cmd("add " + projectName + " package Ninject");
                dotnet.Cmd("add " + projectName + " package Ninject.Extensions.ContextPreservation");
                dotnet.Cmd("add " + projectName + " package Ninject.Extensions.NamedScope");
            }
            else
            {
                dotnet.Cmd("add " + projectName + " reference CocoriCore/backend/CocoriCore.Autofac");
                dotnet.Cmd("add " + projectName + " package Autofac");
            }
        }

    }
}
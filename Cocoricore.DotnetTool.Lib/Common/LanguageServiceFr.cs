using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;

namespace Cocoricore.DotnetTool.Lib
{
    public class LanguageServiceFr : ILanguageService
    {
        public string Domain => "Metier";
        public string Read => "Lecture";
        public string Write => "Ecriture";
        public string Entity => "Entite";
    }
}
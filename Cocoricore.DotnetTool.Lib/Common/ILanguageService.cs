namespace Cocoricore.DotnetTool.Lib
{
    public interface ILanguageService
    {
        string Domain { get; }
        string Read { get; }
        string Write { get; }
        string Entity { get; }
    }
}
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Reflection;
using Mustache;

namespace Cocoricore.DotnetTool.Lib
{
    public class MustacheTool
    {
        private readonly IWorkingDirectory workingDirectory;
        private readonly ILanguageService languageService;
        private readonly IProjectName projectName;
        private readonly MustacheToolConfiguration conf;

        public MustacheTool(
            IWorkingDirectory workingDirectory,
            ILanguageService languageService,
            IProjectName projectName,
            MustacheToolConfiguration conf)
        {
            this.workingDirectory = workingDirectory;
            this.languageService = languageService;
            this.projectName = projectName;
            this.conf = conf;
        }

        public void Generate(string directory, params string[] filenames)
        {
            Generate(null, directory, filenames);
        }

        public void Generate(object customData, string directory, params string[] filenames)
        {

            var compiler = new FormatCompiler() { RemoveNewLines = false };
            var mustacheData = GetData(customData);


            foreach (var f in filenames)
            {
                var fullPath = Path.Combine(directory, f);
                var filename = Path.GetFileName(fullPath);
                var dir = Path.GetDirectoryName(fullPath);
                var stream = this.FindResourceStream(this.conf.Assembly, dir, filename);


                // first Mustache the file name
                var outputFilename = compiler.Compile(fullPath).Render(mustacheData);
                outputFilename = Path.Combine(this.workingDirectory.Directory, outputFilename);

                // then mustache the content
                using (var reader = new StreamReader(stream))
                {
                    var fileContent = reader.ReadToEnd();

                    string result = compiler.Compile(fileContent).Render(mustacheData);

                    if (!Directory.Exists(Path.GetDirectoryName(outputFilename)))
                        Directory.CreateDirectory(Path.GetDirectoryName(outputFilename));
                    File.WriteAllText(outputFilename, result);
                }
            }
        }

        private Stream FindResourceStream(Assembly assembly, string dir, string filename)
        {
            /*
            Generated resource names :
            - There can be only one '.' in resource file name
            - '{', '}' are replaced by _, in directories but not in filenaes 
                ex : Files.__ProjectName__.__Domain__.__Entity_.{{EntityName}}.hbs
            */
            var names = assembly.GetManifestResourceNames();

            var resourceName1 =
                       dir.Replace("/", ".").Replace("{", "_").Replace("}", "_")
                    + (dir == "" ? "" : ".")
                    + filename;

            // try with current name, then add .hbs, then replace extension with .hbs
            var found = names.FirstOrDefault(x => x.EndsWith(resourceName1));
            if (found == null)
                found = names.FirstOrDefault(x => x.EndsWith(resourceName1 + ".hbs"));
            if (found == null)
                found = names.FirstOrDefault(x => x.EndsWith(Path.GetFileNameWithoutExtension(resourceName1) + ".hbs"));
            if (found == null)
                throw new Exception($"Resource not found for {dir}/{filename}");

            return assembly.GetManifestResourceStream(found);
        }

        private ExpandoObject GetData(object customData)
        {
            var data = new ExpandoObject();
            CopyProperties(data, this.languageService);
            CopyProperties(data, customData);
            CopyProperties(data, this.projectName);
            return data;
        }

        private void CopyProperties(IDictionary<string, object> data, object customData)
        {
            if (customData == null)
                return;

            foreach (var property in customData.GetType().GetProperties())
            {
                data.Add(property.Name, property.GetValue(customData));
            }
        }
    }
}
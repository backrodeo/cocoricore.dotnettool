using System;
using System.Diagnostics;

namespace Cocoricore.DotnetTool.Lib
{
    public class Git
    {

        /*
        public Git WorkingDirectory(string dir)
        {
            this.workingDir = dir;
            return this;
        }*/

        public void Clone(string url, string directory)
        {
            var startInfo = new ProcessStartInfo()
            {
                FileName = "git",
                Arguments = "clone " + url + " .",
                WorkingDirectory = directory
            };
            var proc = Process.Start(startInfo);
            proc.WaitForExit();
            if (proc.ExitCode != 0)
                throw new Exception("Git Clone Error : ExitCode=" + proc.ExitCode);
        }

    }
}
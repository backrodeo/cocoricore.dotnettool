using System;
using System.Diagnostics;

namespace Cocoricore.DotnetTool.Lib
{
    public class DotNet
    {
        private string workingDir;

        public DotNet(IWorkingDirectory workingDir)
        {
            this.workingDir = workingDir.Directory;
        }

        public void Try(string arguments, int timeout = 60000)
        {
            Command(this.workingDir, arguments, false, timeout);
        }

        public void Cmd(string arguments, int timeout = 60000)
        {
            Command(this.workingDir, arguments, true, timeout);
        }

        public void Command(string _workingDir, string arguments, bool exceptions, int timeout = 60000)
        {
            var startInfo = new ProcessStartInfo()
            {
                FileName = "dotnet",
                Arguments = arguments,
                WorkingDirectory = _workingDir
            };

            var proc = Process.Start(startInfo);
            proc.WaitForExit(timeout);

            if (exceptions)
            {
                if (!proc.HasExited)
                    throw new Exception("Dotnet has taken too Long");

                if (proc.ExitCode == 1)
                    throw new Exception("Dotnet error : ExitCode: " + proc.ExitCode + " Command: dotnet " + arguments);
            }
        }

    }
}
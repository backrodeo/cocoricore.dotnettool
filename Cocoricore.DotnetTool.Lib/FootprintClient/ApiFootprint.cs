// TODO : mettre une referenc à Cocoricore ?

using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json.Linq;

namespace Cocoricore.DotnetTool.Lib
{
    /* Not used
    public class ApiFootprint
    {
        public IEnumerable<RouteFootprint> Routes;
        public Dictionary<string, Dictionary<string, int>> Enums;
        public Dictionary<string, object> Types;
    }*/

    public class RouteFootprint
    {
        public RouteFootprint(JObject route)
        {
            Method = route.Value<string>("Method");
            Url = route.Value<string>("Url");
            Response = route.Value<string>("Response");
            QueryString = route.Value<JObject>("QueryString");
            Body = route.Value<JObject>("Body");
        }
        public string Url;
        public string Method;
        public string Description;
        public JObject QueryString;
        public string Response;
        public JObject Body;
    }
}
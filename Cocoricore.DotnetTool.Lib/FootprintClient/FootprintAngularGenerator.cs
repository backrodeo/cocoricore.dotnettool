using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Cocoricore.DotnetTool.Lib
{
    public class FootprintAngularGenerator
    {
        public void GenerateClientFromFile(string input, string output)
        {
            var json = File.ReadAllText(input);
            JObject footprint = JObject.Parse(json);
            File.WriteAllText(output, GenerateClient(footprint));
        }

        public string GenerateClient(JObject jsonFootprint)
        {
            return Imports()
            + Alias()
            + ApiServiceStart()
            //+ generateRoutes(footprint)
            + ApiServiceEnd()
            //+ generateTypes(footprint)
            ;
        }


        private string Imports()
        {
            return @"
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
";
        }

        private string Alias()
        {
            return @"
            type guid = string;
    ";
        }

        private string ApiServiceStart()
        {
            return @"
@Injectable()
class ApiService
{

";
        }

        private string ApiServiceEnd()
        {

            return @"
    
    accessToken:string;
    refreshToken:string;

    constructor(private http: HttpClient) { }

    private jsonHttpOptions(httpParams?:HttpParams): any
    {
        var httpOptions = {
            headers: new HttpHeaders({
                'Content-Type':  'application/json',
                'Authorization': this.accessToken
            }),
            params:httpParams
        };
    }
}";
        }
    }
}
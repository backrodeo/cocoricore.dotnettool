using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Cocoricore.DotnetTool.Lib
{
    public class FootprintFetchApiGenerator
    {
        public void GenerateClientFromFile(string input, string output)
        {
            var json = File.ReadAllText(input);
            JObject footprint = JObject.Parse(json);
            File.WriteAllText(output, GenerateClient(footprint));
        }

        public string GenerateClient(JObject jsonFootprint)
        {
            return "";
        }
        public string GenerateTypes(string json)
        {
            JObject footprint = JObject.Parse(json);
            return GenerateTypes(footprint);
        }

        public string GenerateTypes(JObject jsonFootprint)
        {
            var sb = new StringBuilder();
            var typesDic = jsonFootprint.GetValue("Types") as JObject;
            foreach (var p in typesDic.Properties())
            {
                var typeName = p.Name;
                var typeObject = p.Value as JObject;

                sb.AppendLine("class " + typeName + " {");
                foreach (var p2 in typeObject.Properties())
                {
                    sb.AppendLine("    " + p2.Name + ": " + p2.Value.Value<string>() + ";");
                }
                sb.AppendLine("}");
            }
            return sb.ToString();
        }

        public string GenerateRoutes(string json)
        {
            JObject footprint = JObject.Parse(json);
            return GenerateRoutes(footprint);
        }

        public string GenerateRoutes(JObject jsonFootprint)
        {
            var sb = new StringBuilder();
            var routes = jsonFootprint.GetValue("Routes") as JArray;
            foreach (var r in routes)
            {
                sb.AppendLine(this.GenerateRouteDeclaration(new RouteFootprint((JObject)r)));
            }

            sb.AppendLine("    async api(url: any, method: any, _1?: any, _2?: any, _3?: any, _4?: any, _5?: any, _6?: any, _7?: any, _8?: any, _9?: any): Promise<any> {");
            foreach (var r in routes as JArray)
            {
                sb.Append(this.GenerateRouteImplementation(new RouteFootprint((JObject)r)));
            }
            sb.AppendLine("    }");

            return sb.ToString();
        }

        public string GenerateRouteImplementation(RouteFootprint route)
        {
            var url = GetTransformedRouteUrl(route.Url);

            var numUrlParameters = ParametersWithTypeFromUrl(route.Url).Count;
            var queryStringParameters = ParametersWithoutTypeFromObject(route.QueryString);
            var routeBodyParameters = ParametersWithoutTypeFromObject(route.Body);


            string queryParamsStr = null;
            if (route.QueryString != null)
            {
                queryParamsStr = string.Join("",
                    queryStringParameters.Select(
                        (s, i) =>
                        {
                            var paramName = "_" + (numUrlParameters + 1 + i);
                            return "            urlapi.searchParams.append('" + s + "', " + paramName + ");\n";
                        }
                    ));
            };

            string bodyParamsStr = null;
            if (route.Body != null)
            {
                bodyParamsStr = "            let body = new URLSearchParams();\n";
                bodyParamsStr += string.Join("",
                    routeBodyParameters.Select(
                        (s, i) =>
                        {
                            var paramName = "_" + (numUrlParameters + 1 + i);
                            return "            body.append('" + s + "', " + paramName + ");\n";
                        }
                    ));
            };

            var sb = new StringBuilder();
            sb.AppendLine("        if (url == '" + url + "' && method == '" + route.Method + "') {");
            sb.AppendLine("            let urlapi = new URL(" + ToUrlStringExpression(url) + ");");
            if (queryParamsStr != null)
                sb.Append(queryParamsStr);
            if (bodyParamsStr != null)
            {
                sb.Append(bodyParamsStr);
                sb.AppendLine("            let response = await fetch(urlapi, defaultRequestInit(method, body));");
            }
            else
            {
                sb.AppendLine("            let response = await fetch(urlapi, defaultRequestInit(method));");
            }
            sb.AppendLine("            return await response.json();");
            sb.AppendLine("        }");

            return sb.ToString();
        }


        private string GenerateRouteDeclaration(RouteFootprint route)
        {
            var url = GetTransformedRouteUrl(route.Url);
            var parameters = new List<string>();
            parameters.Add("url: '" + url + "'");
            parameters.Add("method: '" + route.Method + "'");
            parameters.AddRange(ParametersWithTypeFromUrl(route.Url));
            parameters.AddRange(ParametersWithTypeFromObject(route.QueryString));
            parameters.AddRange(ParametersWithTypeFromObject(route.Body));

            return "    async api" + "(" + string.Join(", ", parameters) + "): Promise<" + route.Response + ">;";
        }


        private List<string> ParametersWithTypeFromUrl(string url)
        {
            var parameters = url.Split('/').Where(x => x.IndexOf(':') != -1).ToList();
            return parameters.Select(x => x.Replace(":", ": ")).ToList();
        }

        private string GetTransformedRouteUrl(string url)
        {
            var segments = url.Split('/').Where(x => x != "").Select(p =>
            {
                var i = p.IndexOf(':');
                if (i != -1)
                    return ':' + p.Substring(0, i);
                else
                    return p;
            }).ToList();
            return '/' + string.Join("/", segments);
        }

        private List<string> ParametersWithTypeFromObject(JObject queryOrBody)
        {
            if (queryOrBody == null)
                return new List<string>();

            var properties = queryOrBody.Properties();
            return properties.Select(v => v.Name + ": " + v.Value.ToString()).ToList();
        }

        private List<string> ParametersWithoutTypeFromObject(JObject queryOrBody)
        {
            if (queryOrBody == null)
                return new List<string>();

            var values = queryOrBody.Properties();
            return values.Select(v => v.Name).ToList();
        }

        private string ToUrlStringExpression(string url)
        {
            var indexParam = 1;
            var segments = url.Split('/').Where(x => x != "").Select(
                p =>
                {
                    var i = p.IndexOf(':');
                    if (i != -1)
                        return "'/' + " + '_' + (indexParam++);
                    else
                        return "'/" + p + "'";
                });
            return string.Join(" + ", segments);
        }
    }
}
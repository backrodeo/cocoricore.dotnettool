﻿namespace Cocoricore.DotnetTool.Lib
{
    public class CocoriCoreDependencyConfiguration
    {
        public bool Package;
        public bool Git;
        public string Directory;
    }
}
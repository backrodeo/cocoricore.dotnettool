namespace Cocoricore.DotnetTool.Lib
{
    public enum InjectionLibrary
    {
        Autofac,
        Ninject
    }
}
namespace Cocoricore.DotnetTool.Lib
{
    public class GeneratorConfigurationBase
    {
        public string GeneratorName;

        public GeneratorConfigurationBase(string generatorName)
        {
            GeneratorName = generatorName;
        }
    }
}

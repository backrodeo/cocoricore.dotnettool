﻿using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using Cocoricore.DotnetTool.EtienneQuickStart;
using Cocoricore.DotnetTool.Lib;
using McMaster.Extensions.CommandLineUtils;
using Newtonsoft.Json;

namespace Cocoricore.DotnetTool.Exe
{
    public class Program
    {
        public static int Main(string[] args)
        {
            var app = new CommandLineApplication
            {
                Name = "CocoriCore CLI",
                Description = "A CLI to manage cocoricore projects",
            };

            app.HelpOption(inherited: true);
            app.Command("new", newCmd =>
            {
                newCmd.OnExecute(() =>
                {
                    Console.WriteLine("Specify a subcommand");
                    newCmd.ShowHelp();
                    return 1;
                });

                newCmd.Command("EtienneQuickStart", cmd =>
                {
                    cmd.Description = "Create a project of type EtienneQuickStart";
                    var projectName = cmd.Argument("projectName", "Name of the project").IsRequired();
                    //var directory = cmd.Argument("directory", "Parent of the root directory of the project");
                    cmd.OnExecute(() =>
                    {
                        var language = Prompt.GetString("language (en|fr) ?", "en");
                        var cocoricoreSource = Prompt.GetString("cocoricore source (git| 'a path like '/home/user/cocoricore') ?", "git");

                        var conf = new EtienneQuickStart.Configuration()
                        {
                            ProjectName = projectName.Value,
                            Language = language == "fr" ? Language.Fr : Language.En,
                            CocoriCore = {
                                Git = cocoricoreSource == "git",
                                Directory = cocoricoreSource == "git" ? null : cocoricoreSource
                            }
                        };

                        var dir = Path.Combine(Environment.CurrentDirectory, projectName.Value);
                        DirectoryExtension.EnsureEmpty(dir);

                        var generator = Injection.Instantiate(conf, dir);
                        generator.New();

                        File.WriteAllText(Path.Combine(dir, "cocoricore.json"), JsonConvert.SerializeObject(conf));
                    });
                });
            });

            app.Command("add", addCmd =>
            {
                addCmd.OnExecute(() =>
                {
                    Console.WriteLine("Specify a subcommand");
                    addCmd.ShowHelp();
                    return 1;
                });

                addCmd.Command("query", addQueryCmd =>
                {
                    addQueryCmd.Description = "Add a query";
                    var queryName = addQueryCmd.Argument("queryName", "Name of the query").IsRequired();
                    var addEntity = addQueryCmd.Option("--entity", "Add Entity", CommandOptionType.SingleOrNoValue);

                    addQueryCmd.OnExecute(() =>
                    {
                        // try to read cocoricore.json
                        var dir = Path.Combine(Environment.CurrentDirectory);
                        if (!File.Exists(Path.Combine(dir, "cocoricore.json")))
                            throw new Exception("cocoricore.json not found !");
                        var confFile = File.ReadAllText(Path.Combine(dir, "cocoricore.json"));
                        var confBase = JsonConvert.DeserializeObject<GeneratorConfigurationBase>(confFile);

                        string generatorName = confBase.GeneratorName;
                        if (generatorName == new EtienneQuickStart.Configuration().GeneratorName)
                        {
                            Console.WriteLine("Create query " + queryName.Value);

                            var conf = JsonConvert.DeserializeObject<EtienneQuickStart.Configuration>(confFile);
                            var generator = EtienneQuickStart.Injection.Instantiate(conf, dir);
                            generator.AddQuery(queryName.Value); //, addEntity.HasValue());
                        }
                    });
                });

                addCmd.Command("command", addCommandCmd =>
                {

                    var commandName = addCommandCmd.Argument("commandName", "Name of the command").IsRequired();
                    var addEntity = addCommandCmd.Option("--entity", "Add Entity", CommandOptionType.SingleOrNoValue);

                    addCommandCmd.OnExecute(() =>
                    {
                        // try to read cocoricore.json
                        var dir = Path.Combine(Environment.CurrentDirectory);
                        if (!File.Exists(Path.Combine(dir, "cocoricore.json")))
                            throw new Exception("cocoricore.json not found !");
                        var confFile = File.ReadAllText(Path.Combine(dir, "cocoricore.json"));
                        var confBase = JsonConvert.DeserializeObject<GeneratorConfigurationBase>(confFile);

                        string generatorName = confBase.GeneratorName;
                        if (generatorName == new EtienneQuickStart.Configuration().GeneratorName)
                        {
                            Console.WriteLine("Create command " + commandName.Value);
                            var conf = JsonConvert.DeserializeObject<EtienneQuickStart.Configuration>(confFile);
                            var generator = EtienneQuickStart.Injection.Instantiate(conf, dir);
                            generator.AddCommand(commandName.Value); //, addEntity.HasValue());
                        }
                    });
                });
            });

            app.Command("footprint", footprintCmd =>
            {
                footprintCmd.OnExecute(() =>
                {
                    Console.WriteLine("Specify a subcommand");
                    footprintCmd.ShowHelp();
                    return 1;
                });

                footprintCmd.Command("fetchApi", addQueryCmd =>
                {
                    var footprintGen = new FootprintFetchApiGenerator();
                    // dotnet test --filter ...
                });
            });

            app.OnExecute(() =>
            {
                Console.WriteLine("Specify a subcommand");
                app.ShowHelp();
                return 1;
            });

            app.UsePagerForHelpText = false;
            return app.Execute(args);
        }
    }
}
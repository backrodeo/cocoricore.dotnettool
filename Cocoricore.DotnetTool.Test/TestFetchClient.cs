using System;
using System.Diagnostics;
using System.IO;
using Cocoricore.DotnetTool.Lib;
using Xunit;
using FluentAssertions;
using System.Linq;

namespace Cocoricore.DotnetTool.Test
{
    public class TestFetchClient
    {
        [Fact]
        public void NoQueryNoBody()
        {
            TestRoutes("NoQueryNoBody");
        }
        [Fact]
        public void GetWithQuery()
        {
            TestRoutes("GetWithQuery");
        }

        [Fact]
        public void GetWithUrlParameter()
        {
            TestRoutes("GetWithUrlParameter");
        }

        [Fact]
        public void GetWithUrlParameterAndQuery()
        {
            TestRoutes("GetWithUrlParameterAndQuery");
        }

        [Fact]
        public void PostWithBody()
        {
            TestRoutes("PostWithBody");
        }
        [Fact]
        public void PostWithUrlParameter()
        {
            TestRoutes("PostWithUrlParameter");
        }
        [Fact]
        public void PostWithUrlParameterAndBody()
        {
            TestRoutes("PostWithUrlParameterAndBody");
        }
        [Fact]
        public void Types()
        {
            (string footprint, string api) = GetResources("Types");
            var generator = new FootprintFetchApiGenerator();
            var routes = generator.GenerateTypes(footprint);
            routes.Should().Be(api);
        }

        private void TestRoutes(string testName)
        {
            (string footprint, string api) = GetResources(testName);
            var generator = new FootprintFetchApiGenerator();
            var routes = generator.GenerateRoutes(footprint);
            Console.WriteLine(routes);
            routes = "class " + testName + " {\n\n"
                    + routes
                    + "}";
            routes.Should().Be(api);
        }

        private (string, string) GetResources(string testName)
        {
            string footprint;
            string api;

            var assembly = this.GetType().Assembly;
            var names = assembly.GetManifestResourceNames();
            var stream = assembly.GetManifestResourceStream(names.First(x => x.Contains($"{testName}.footprint.json")));
            using (var reader = new StreamReader(stream))
            {
                footprint = reader.ReadToEnd();
            }
            stream = assembly.GetManifestResourceStream(names.First(x => x.Contains($"{testName}.api.ts")));
            using (var reader = new StreamReader(stream))
            {
                api = reader.ReadToEnd();
            }
            return (footprint, api);
        }
    }
}
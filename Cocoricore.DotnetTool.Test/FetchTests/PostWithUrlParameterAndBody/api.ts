class PostWithUrlParameterAndBody {

    async api(url: '/users/:id/products', method: 'POST', id: guid, Type: string, Price: string): Promise<guid>;
    async api(url: any, method: any, _1?: any, _2?: any, _3?: any, _4?: any, _5?: any, _6?: any, _7?: any, _8?: any, _9?: any): Promise<any> {
        if (url == '/users/:id/products' && method == 'POST') {
            let urlapi = new URL('/users' + '/' + _1 + '/products');
            let body = new URLSearchParams();
            body.append('Type', _2);
            body.append('Price', _3);
            let response = await fetch(urlapi, defaultRequestInit(method, body));
            return await response.json();
        }
    }
}
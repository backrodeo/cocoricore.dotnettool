class NoQueryNoBody {

    async api(url: '/version', method: 'GET'): Promise<string>;
    async api(url: any, method: any, _1?: any, _2?: any, _3?: any, _4?: any, _5?: any, _6?: any, _7?: any, _8?: any, _9?: any): Promise<any> {
        if (url == '/version' && method == 'GET') {
            let urlapi = new URL('/version');
            let response = await fetch(urlapi, defaultRequestInit(method));
            return await response.json();
        }
    }
}
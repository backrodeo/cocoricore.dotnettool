class MyComponent {
    async init() {
        var apiService = new GetWithQuery();
        await apiService.api('/profiles', 'GET');
    }
}
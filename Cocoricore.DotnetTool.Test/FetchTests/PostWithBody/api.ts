class PostWithBody {

    async api(url: '/profiles', method: 'POST', FirstName: string, LastName: string): Promise<guid>;
    async api(url: any, method: any, _1?: any, _2?: any, _3?: any, _4?: any, _5?: any, _6?: any, _7?: any, _8?: any, _9?: any): Promise<any> {
        if (url == '/profiles' && method == 'POST') {
            let urlapi = new URL('/profiles');
            let body = new URLSearchParams();
            body.append('FirstName', _1);
            body.append('LastName', _2);
            let response = await fetch(urlapi, defaultRequestInit(method, body));
            return await response.json();
        }
    }
}
class PostWithUrlParameter {

    async api(url: '/users/:id/products', method: 'POST', id: guid): Promise<guid>;
    async api(url: any, method: any, _1?: any, _2?: any, _3?: any, _4?: any, _5?: any, _6?: any, _7?: any, _8?: any, _9?: any): Promise<any> {
        if (url == '/users/:id/products' && method == 'POST') {
            let urlapi = new URL('/users' + '/' + _1 + '/products');
            let response = await fetch(urlapi, defaultRequestInit(method));
            return await response.json();
        }
    }
}

type guid = string;

function defaultRequestInit(method: string, body: URLSearchParams = null): RequestInit {
    var headers = new Headers();
    var init: RequestInit = {
        method: method,
        headers: headers,
        mode: 'cors',
        body: body,
        cache: 'default'
    };
    return init;
}

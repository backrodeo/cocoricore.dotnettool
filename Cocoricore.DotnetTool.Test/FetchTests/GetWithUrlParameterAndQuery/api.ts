class GetWithUrlParameterAndQuery {

    async api(url: '/users/:id/products', method: 'GET', id: guid, Search: string): Promise<Product[]>;
    async api(url: any, method: any, _1?: any, _2?: any, _3?: any, _4?: any, _5?: any, _6?: any, _7?: any, _8?: any, _9?: any): Promise<any> {
        if (url == '/users/:id/products' && method == 'GET') {
            let urlapi = new URL('/users' + '/' + _1 + '/products');
            urlapi.searchParams.append('Search', _2);
            let response = await fetch(urlapi, defaultRequestInit(method));
            return await response.json();
        }
    }
}
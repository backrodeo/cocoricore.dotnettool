class GetWithQuery {

    async api(url: '/users/profiles', method: 'GET', Search: string): Promise<Profile[]>;
    async api(url: any, method: any, _1?: any, _2?: any, _3?: any, _4?: any, _5?: any, _6?: any, _7?: any, _8?: any, _9?: any): Promise<any> {
        if (url == '/users/profiles' && method == 'GET') {
            let urlapi = new URL('/users' + '/profiles');
            urlapi.searchParams.append('Search', _1);
            let response = await fetch(urlapi, defaultRequestInit(method));
            return await response.json();
        }
    }
}
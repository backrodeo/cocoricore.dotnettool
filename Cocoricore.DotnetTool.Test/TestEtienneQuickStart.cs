using System;
using System.Diagnostics;
using System.IO;
using Cocoricore.DotnetTool.Lib;
using Xunit;

namespace Cocoricore.DotnetTool.Test
{
    public class TestEtienneQuickStart
    {

        [Fact]
        public void TestEtienneQuickStartFrench()
        {
            var generator = InitEtienneQuickStart("TestEtienneQuickStartFrenchDomain",
            x =>
            {
                x.DomainOnly = true;
                x.Language = Language.Fr;
            });
            generator.AddEntity("Produit");
            generator.AddCommand("Produit");
            generator.AddQuery("Produit");
            generator.AddQuery("RechercherProduit");
        }

        [Fact]
        public void TestEtienneQuickStartFrenchNew()
        {
            var generator = InitEtienneQuickStart("TestEtienneQuickStartFrench",
            x =>
            {
                x.DomainOnly = false;
                x.Language = Language.Fr;
            });
            generator.New();
        }

        /*
                public void TestEtienneQuickStartEnglish()
                {
                    InitEtienneQuickStart("TestEtienneQuickStartEnglish", Language.En);

                    // create a mirror to avoid git multiple downloads


                    //var dotnet = new DotNet().WorkingDirectory(dir);
                    //dotnet.Cmd("restore");
                    //dotnet.Cmd("build", 120000);

                    // TODO Check if compiled dll exists
                    // CheckDllExists(basedir, projectName);
                }
        */

        private EtienneQuickStart.Generator InitEtienneQuickStart(string testName, Action<EtienneQuickStart.Configuration> configure)
        {
            var mirrorDir = Path.GetFullPath(Path.Join("..", "CocoriCoreMirror"));
            MirrorCocoricore(mirrorDir);

            var conf = new EtienneQuickStart.Configuration()
            {
                ProjectName = testName,
                InjectionLibrary = InjectionLibrary.Ninject,
                CocoriCore = {
                    Directory = mirrorDir
                },
                Language = Language.En,
                DomainOnly = true
            };
            configure(conf);

            var dir = Path.GetFullPath(Path.Join("..", testName));
            DirectoryExtension.EnsureEmpty(dir);

            return EtienneQuickStart.Injection.Instantiate(conf, dir);
        }


        private void MirrorCocoricore(string mirrorDir)
        {
            if (!Directory.Exists(mirrorDir))
            {
                Directory.CreateDirectory(mirrorDir);

                var startInfo = new ProcessStartInfo()
                {
                    FileName = "git",
                    Arguments = "clone https://bitbucket.org/llafitte007/cocoricore.git .",
                    WorkingDirectory = mirrorDir
                };

                var proc = Process.Start(startInfo);
                proc.WaitForExit();
            }
        }

        private void CheckDllExists(string basedir, string projectName)
        {
            //Assert.True(File.Exists(Path.Combine(basedir, "/bin/debug")))
        }
    }
}

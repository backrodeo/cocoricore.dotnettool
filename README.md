# Introduction

CocoriCore DotnetTool is a CLI for CocoriCore solution generation. You can switch easily between CocoriCore NuGet package and CocoriCore source code. And you have multiple options :
- BigProject configuration
- EtienneQuickStart configuration

## Examples


```
cocoricore new EtienneQuickStart MyProject
cocoricore add entity BlogPost
cocoricore add query BlogPost
cocoricore add command BlogPost
cocoricore footprint fetchApi
```

# Installation

```
dotnet pack
dotnet tool install --global --add-source Cocoricore.DotnetTool.Exe/nupkg Cocoricore.DotnetTool.Exe
```

